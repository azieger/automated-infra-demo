variable "do_token" {}
variable "ssh_fingerprint" {
  default = "0c:ad:43:eb:a2:c0:34:d0:0a:18:18:09:52:c7:33:07"
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_droplet" "my_first_droplet" {
  image              = "centos-7-x64"
  name               = "my-droplet"
  region             = "nyc1"
  size               = "s-1vcpu-2gb"
  monitoring         = true
  private_networking = true
  ssh_keys           = [ var.ssh_fingerprint ]
  user_data          = file("./scripts/user-data.sh")
}

