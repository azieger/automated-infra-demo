# Pre-requisits 

* Install terraform > 0.12
* Install docker
* Digital ocean account
* Digital ocean API key

# Terraform

`terraform init`
`terraform plan -var "do_token=$DEOM_DO_TOKEN"`
`terraform apply plan.out`

# Docker

`docker login registry.gitlab.com`
`docker build -t registry.gitlab.com/azieger/automated-infra-demo .`
`docker push registry.gitlab.com/azieger/automated-infra-demo`

## A few useful commands
`docker image ls`
Start a container from an image and execute commands in it
`docker run --rm -it <image_id>`
`docker run --rm -it --entrypoint=/bin/sh <image_id>`
`docker logs <container_id>`
`docker image ls`
`docker ps -a`
Get inside a running container and execute commands
`docker exec -it <container_id> /bin/sh` 